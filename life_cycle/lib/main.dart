import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Page2(),
    );
  }
}

//StatelessWidget  build
//StatefulWidget--createState--build

//组件树
//StatelessWidget --->Page1（）
//StatefulWidget-->page2.state.build()

//state 属性
class Page1 extends StatelessWidget {
  const Page1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Page2 extends StatefulWidget {
  const Page2({Key? key}) : super(key: key);

  @override
  State<Page2> createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  int counter = 0;
  @override
  void initState() {
    super.initState();
    print(' state init....');
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print(' state didChangeDependencies....');
  }

  @override
  void reassemble() {
    //调试
    super.reassemble();
    print(' state reassemble....');
  }

  @override
  void didUpdateWidget(covariant Page2 oldWidget) {
    super.didUpdateWidget(oldWidget);
    print(' state didUpdateWidget....');
  }

  @override
  void deactivate() {
    super.deactivate();
    print(' state deactivate....');
  }

  @override
  void dispose() {
    super.dispose();
    print(' state dispose....');
  }

  @override
  Widget build(BuildContext context) {
    print('  build....');
    return Scaffold(
      appBar: AppBar(title: Text('唐果大龙')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              setState(() {
                counter++;
              });
            },
            icon: Icon(Icons.add),
            label: Text('加${counter}')),
      ),
    );
  }
}
