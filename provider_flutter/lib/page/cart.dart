import 'package:flutter/material.dart';
import 'package:provider_flutter/model/cart_model.dart';
import 'package:provider/provider.dart';
import 'catalog.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    CartModel cartModel = context.watch<CartModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('购物车'),
      ),
      body: ListView.builder(
          itemCount: cartModel.cart.length,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Image.network(
                'http://www.tangguoketang.com/img/banner01.7c3681ef.png',
                width: 100,
              ),
              title: Text('${cartModel.cart[index].name}'),
              trailing: IconButton(
                icon: Icon(Icons.remove),
                onPressed: () {
                  setState(() {
                    cartModel.remove(cartModel.cart[index]);
                  });
                },
              ),
            );
          }),
    );
  }
}
