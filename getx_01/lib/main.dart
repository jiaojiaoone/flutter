import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Controller c = Get.put(Controller());
    return Scaffold(
      appBar: AppBar(
          title: Obx(
        () => Text('${c.count}'),
      )),
      body: Center(
        child: ElevatedButton(
          child: Text('goto other'),
          onPressed: () {
            //other
            Get.to(Other());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          c.increment();
        },
      ),
    );
  }
}

class Other extends StatelessWidget {
  const Other({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Controller c = Get.find();
    return Scaffold(
      appBar: AppBar(title: Text('other')),
      body: Center(
        child: Text('${c.count}'),
      ),
    );
  }
}

class Controller extends GetxController {
  var count = 0.obs;
  increment() => count++;
}
