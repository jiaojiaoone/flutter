import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scaffold Page'),
        //leading: Icon(Icons.home),
        actions: [
          Icon(Icons.share),
          Icon(Icons.shopping_bag),
        ],
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: [
          Container(color: Colors.red),
          Container(color: Colors.yellow),
          Container(color: Colors.blue),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
                decoration: BoxDecoration(color: Colors.lightBlueAccent),
                child: Center(
                  child: SizedBox(
                    width: 60,
                    height: 60,
                    child: CircleAvatar(
                      child: Image.network(
                          'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnimg.ws.126.net%2F%3Furl%3Dhttp%253A%252F%252Fdingyue.ws.126.net%252F2021%252F0314%252F94ad46dbj00qpy1do0021d200rs00rsg008t008t.jpg%26thumbnail%3D650x2147483647%26quality%3D80%26type%3Djpg&refer=http%3A%2F%2Fnimg.ws.126.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1653054545&t=0adf1be7ba8035ba43e71dd384705d91'),
                    ),
                  ),
                )),
            ListTile(
              title: Text('订单'),
              leading: Icon(Icons.share),
            ),
            ListTile(
              title: Text('课程'),
              leading: Icon(Icons.share),
            ),
            ListTile(
              title: Text('设置'),
              leading: Icon(Icons.share),
            ),
            ListTile(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart), label: '购物车'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: '我的'),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          setState(() {
            this._currentIndex = 2;
          });
        },
      ),
    );
  }
}
